// import thư viện mongose 
const mongoose = require("mongoose");
//improt diceHistoryModel 
const diceHistoryModel = require("../model/diceHistoryModel");
//Tạo createDiceHistory
const createDiceHistory = (req,res) =>{
    //B1: thu thập dữ liệu từ req
    let body = req.body
    //B2 Kiểm tra dữ liệu
    if(body.user){
        return res.status(400).json({
            message: "user không hợp lệ"
        })
    }
    if(!Number.isInteger(body.dice) || body.dice < 0) {
        return res.status(400).json({
          message: 'Dữ liệu dice không đúng'
        })
      }
    // B3 Gọi model thực hiện các thao tác nghiệp vụ
    //thu thập dữ liệu
    let newDice = {
        _id: mongoose.Types.ObjectId(),
        user: body.user,
        dice: body.dice
    }
    //tạo mới dữ liệu
    diceHistoryModel.create(newDice,(err,data)=>{
        if(err){
            return res.status(500).json({
                message: err.message
            })
        }
        else{
            return res.status(201).json({
                message:"Tạo mới thành công",
                Dice : data
            })
        }
    })
};
//Tạo function getAllDiceHistory
const getAllDiceHistory = (req,res)=>{
     //B1: thu thập dữ liệu từ req
    //B2: validate dữ liệu
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    diceHistoryModel.find((err,data)=>{
        if(err){
            return res.status(500).json({
                 message: err.message
             })
         }
         else{
             return res.status(201).json({
                 message:"Tải toàn bộ dữ liệu thành công",
                 Data: data
             })
         }
    })
};
// Tạo function getDiceHistoryById
const getDiceHistoryById = (req,res)=>{
    //B1: thu thập dữ liệu từ req
    let id = req.params.diceHistoryId
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
          message: 'id không hợp lệ'
        })
      }
  //B3: Gọi model thực hiện các thao tác nghiệp vụ
  diceHistoryModel.findById(id,(err,data)=>{
    if(err){
        return res.status(500).json({
             message: err.message
         })
     }
     return res.status(201).json({
        message:"Tải dữ liệu thành công thông qua Id",
        Data: data
       })
  })
};
// Tạo function updateDiceHistoryById
const updateDiceHistoryById = (req,res)=>{
    //B1: thu thập dữ liệu từ req
    let id = req.params.diceHistoryId
    let body = req.body
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
          message: 'id không hợp lệ'
        })
      }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    let DiceHistoryUpdate = {
        user: body.user,
        dice: body.dice
      }
    diceHistoryModel.findByIdAndUpdate(id,DiceHistoryUpdate,(err,data)=>{
        if(err){
            return res.status(500).json({
                 message: err.message
             })
         }
         else{
             return res.status(200).json({
                 message:"Cập nhật dữ liệu thành công bằng ID",
                 Data: data
             })
         }
    })
};
// Tạo function deleteDiceHistoryById
const deleteDiceHistoryById =(req,res)=>{
//B1: thu thập dữ liệu từ req
let id = req.params.diceHistoryId
//B2: validate dữ liệu
if(!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).json({
      message: 'id không hợp lệ'
    })
  }
//B3: Gọi model thực hiện các thao tác nghiệp vụ
diceHistoryModel.findByIdAndDelete(id,(err,data)=>{
    if(err){
        return res.status(500).json({
             message: err.message
         })
     }
     else{
         return res.status(200).json({
             message:"Thành công xoá dữ liệu thông qua ID",
             Data: data
            })
        }
    })
}

//exprot controller
module.exports = {
   createDiceHistory,
   getAllDiceHistory,
   getDiceHistoryById,
   updateDiceHistoryById,
   deleteDiceHistoryById
}
