//Khai báo một thư viện express
const express = require ('express');
// improt middlerware 
const middlerware = require("../middleware/middleware");
//Khai báo controller
const voucherController = require("../controller/voucherController");
//Tạo Router
const voucherRouter = express.Router();
voucherRouter.post("/vouchers",middlerware.Middeware,voucherController.createVoucher);
voucherRouter.get("/vouchers",middlerware.Middeware,voucherController.getAllVoucher);
voucherRouter.get("/vouchers/:voucherId",middlerware.Middeware,voucherController.getVoucherById);
voucherRouter.put("/vouchers/:voucherId",middlerware.Middeware,voucherController.updateVoucherById);
voucherRouter.delete("/vouchers/:voucherId",middlerware.Middeware,voucherController.deleteUserById);

// exprot router
module.exports= {voucherRouter}